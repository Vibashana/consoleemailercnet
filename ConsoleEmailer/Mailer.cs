﻿using System;
using System.Net;
using System.Net.Mail;

namespace EmailSmtp
{
  class Program
  {
    static void Main (string[] args)
    {
      try
      {
        // Credentials
        var credentials = new NetworkCredential ("MyEmail@gmail.com", "MyEmailPassword");

        // Mail message
        var mail = new MailMessage ()
        {
          From = new MailAddress ("MyEmail@gmail.com"),
          Subject = "Email Enquiry",
          Body = "Email body goes here."
        };

        mail.To.Add (new MailAddress ("MyEmail@gmail.com"));

        // Smtp client
        var client = new SmtpClient ()
        {
          Port = 587, // TLS Port or 465 for SSL
          DeliveryMethod = SmtpDeliveryMethod.Network,
          UseDefaultCredentials = false,
          Host = "smtp.gmail.com",
          EnableSsl = true,
          Credentials = credentials
        };

        // Send it...         
        client.Send (mail);
      }
      catch (Exception ex)
      {
        Console.WriteLine ("Error in sending email: " + ex.Message);
        Console.ReadKey ();
        return;
      }

      Console.WriteLine ("Email sccessfully sent");
      Console.ReadKey ();
    }
  }
}